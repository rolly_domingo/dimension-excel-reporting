angular.module('reportingApp.reportingController', ['cp.ngConfirm'])
.constant('PATH', '\\PI-SRVR\EREC')
// .constant('HOST', 'http://calibr8.ddns.net/PIExcelReporting/EREConfig')
.constant('HOST', 'http://10.10.8.105/EREConfiguration/EREConfig')
.controller('testCtrl', ['$scope', 'httpService', '$ngConfirm', 'PATH', 'toastr', '$timeout', function ($scope, httpService, $ngConfirm, PATH, toastr, $timeout) {
	
	httpService.loading('show');

	$scope.delivery_types = { //These are the options that will be displayed in delivery types dropdown
		'Email Delivery Channel' : 'Email',
		'Folder Delivery Channel' : 'File Location',
		'Sharepoint Delivery Channel' : 'SharePoint Document Library',
		'Originating Event Frame Delivery Channel' : 'Event Frame'
	};

	$scope.trigger_types = { //These are the options that will be displayed in trigger types dropdown
		'Day Time Trigger' : 'Time - Selected Days',
		'Event Frame Based Trigger' : 'Event Frame',
		'Period Offset Trigger' : 'Time - Period & Offset',
		'Condition Based Trigger' : 'Condition',
	};

	//this request will be triggered on the first page load
	httpService.getInitialData().then(function (output) { //HTTP request for retrieving the initial data, function is pace at "reporting-service.js"
		$scope.reports = [];

		var element_list = output.data.elementList;
		var enum_list = output.data.enumList;

		$scope.reports = output.data.reportList;

		httpService.loading('hide');

		if($scope.reports.length > 0) { //Select the first report record if there is 
			$scope.selected_report = $scope.reports[0];
			$scope.selected_report_idx = 0;
			$scope.selectReport($scope.selected_report, 0);
		}
		
		$scope.element_list = {};

		for(i in element_list) { //Grouping the elements by category and store it to element_list object valriable
			$scope.element_list[element_list[i].listCategory] = element_list[i].elementList; 
		} 


		$scope.enum_list = {};
		for(e in enum_list) {  //Grouping enum list
			if(!$scope.enum_list[enum_list[e].enumType]) {
				$scope.enum_list[enum_list[e].enumType] = [];
			}
			$scope.enum_list[enum_list[e].enumType] = enum_list[e].enumAttribute;
		}
	}, function (err) {
		httpService.loading('hide');
	});


	$scope.addReport = function() {
		if($scope.generalForm.$dirty || $scope.triggerForm.$dirty || $scope.deliveryForm.$dirty) { //If any fields in the form has been modified
			var confirm = $ngConfirm({
	            title: false,
	            content: '<strong>This will discard your inputs. <br/> Are you sure you want to continue?</strong>',
	            type: 'red',
	            buttons: {
	                Yes: {
	                    action: function (scope, button) {
	                        $scope.generalForm.$setPristine(); //change the status of the general form to unmodified
	                        $scope.triggerForm.$setPristine(); //change the status of the trigger form to unmodified
	                        $scope.deliveryForm.$setPristine(); //change the status of the delivery form to unmodified

	                        $scope.report = {}; //creating report object
	                        
	                        $scope.addReportToTable();
	                    },
	                    btnClass: 'btn-danger'
	                },
	                No: {}
	            }
	        });
		}
		else {
			$scope.report  = {
				id : 0,
				name : '',
				idx : 0
			};

			$scope.addReportToTable();
		}
	}

	$scope.addReportToTable = function () {
		if($scope.report.id == 0) {
			var report_obj = {
		                    	id : '0',
		                    	name : $scope.report.name,
		                    	attributes : [
		                    		{id : '0', name : 'Active Sheet After Export', type : 'String', value: ''},
		                    		{id : '0', name : 'Destination File Name', type : 'String', value: ''},
		                    		{id : '0', name : 'IsEnabled', type : 'Boolean', value: 'True'},
		                    		{id : '0', name : 'Sheet Type', type : 'EnumerationValue', value: '0'},
		                    		{id : '0', name : 'Sheets To Export', type : 'String', value: ''},
		                    		{id : '0', name : 'Source Spreadsheet Location', type : 'String', value: ''},
		                    		{id : '0', name : 'Type', type : 'EnumerationValue', value: '0'}
		                    	],
		                    	triggerTypes : '',
		                    	deliveryTypes : '',
		                    	triggers : [],
		                    	delivery : []
		                    };
		    
			$scope.reports.push(report_obj);
		                    
		    //make the newly added report selected
		    var report_idx = $scope.reports.length - 1;
			$scope.selected_report = $scope.reports[report_idx];
			$scope.selected_report_idx = report_idx;

			$scope.trigger = null;
			$scope.delivery = null;

			$scope.has_pending_report = true; //For pending validation	
		}
		else {
			$scope.reports[$scope.report.idx].name = $scope.report.name; //Changing the name of the edited report
		}
		

		$('#modalAddReport').modal('hide');

		$scope.scrollToBottom('report-table');
	}


	$scope.reportHover = function (idx) {
		$scope.report_hover_idx = idx;
	}

	$scope.reportHoverout = function () {
		$scope.report_hover_idx = null;
	}

	$scope.selectReport = function (r, idx) {
		if($scope.generalForm.$dirty || $scope.triggerForm.$dirty || $scope.deliveryForm.$dirty) { //If any forms in the tabs has been modified
			var confirm = $ngConfirm({
	            title: false,
	            content: '<strong>This will discard your inputs. <br/> Are you sure you want to continue?</strong>',
	            type: 'red',
	            buttons: {
	                Yes: {
	                    action: function (scope, button) {
	                    	if($scope.selected_report.id == '0' || $scope.selected_report.id == 0) { //Means the selected report hasn't been saved
	                    		$scope.reports.splice($scope.selected_report_idx, 1); //Remove the newly added Report
	                    		$scope.has_pending_report = false; //Enable Add button
	                    	}

	                    	$scope.reports[$scope.selected_report_idx] = $scope.original_report;

	                        $scope.selected_report = r;
	                        $scope.selected_report_idx = idx;
	                        $scope.original_report = angular.copy(r); //Save the original Report Details

	                        //Reset the forms
	                        $scope.generalForm.$setPristine();
	                        $scope.triggerForm.$setPristine(); 
	                        $scope.deliveryForm.$setPristine();
	                    },
	                    btnClass: 'btn-danger'
	                },
	                No: {}
	            }
	        });
		}
		else if($scope.has_pending_report) { //If there is an added report that is not yet saved
			var confirm = $ngConfirm({
	            title: false,
	            content: '<strong>This will discard your inputs. <br/> Are you sure you want to continue?</strong>',
	            type: 'red',
	            buttons: {
	                Yes: {
	                    action: function (scope, button) {
	                    	$scope.reports.splice($scope.selected_report_idx, 1); //Deleting the unsaved report 	

	                        $scope.selected_report = r;
	                        $scope.selected_report_idx = idx;
	                        $scope.generalForm.$setPristine();
	                        $scope.triggerForm.$setPristine(); 
	                        $scope.deliveryForm.$setPristine();
	                        $scope.has_pending_report = false;
	                    },
	                    btnClass: 'btn-danger'
	                },
	                No: {}
	            }
	        });
		}
		else {
			$scope.selected_report = r;
			$scope.selected_report_idx = idx;

			$scope.original_report = angular.copy(r); //Save the original Report Details
		}

		if(r.delivery.length > 0) { //Automatically Select the first delivery record if there are any
			$scope.delivery = r.delivery[0];
			$scope.delivery_idx = 0;
		}
		else {
			$scope.delivery = null;
			$scope.delivery_idx = -1;
		}

		if(r.triggers.length > 0) { //Automatically Select the first Trigger Record if there are any
			$scope.trigger = r.triggers[0];
			$scope.trigger_idx = 0;
		}
		else {
			$scope.trigger = null;
			$scope.trigger_idx = -1;
		}
	}

	$scope.deleteReport = function (idx) {
		var confirm = $ngConfirm({
            title: false,
            content: '<strong>Are you sure?</strong>',
            type: 'red',
            buttons: {
                Yes: {
                    action: function (scope, button) {
                    	if($scope.reports[idx].id == 0) {
                        	$scope.has_pending_report = false;
                        	$scope.reports.splice(idx, 1);

                        	if($scope.reports.length > 0) {
                        		$scope.selectReport($scope.reports[0]);
                        	}
                        	else {
                        		$scope.delivery = null;
                        		$scope.delivery_idx = -1;

                        		$scope.trigger = null;
                        		$scope.trigger_idx = -1;
                        	}
                        }
                        else {
	                    	var web_id = $scope.reports[idx].webId;

	                    	httpService.loading('show');
	                    	httpService.deleteReport(web_id).then(function (output) {
	                    		if(output.data.status == 'success') {
	                    			$scope.selected_report = null;
		                        	$scope.reports.splice(idx, 1);
		                        	httpService.loading('hide');

		                        	toastr.success('Report has been Deleted', 'Success!');

		                        	if($scope.reports.length > 0) {
		                        		$scope.selectReport($scope.reports[0]);
		                        	}
		                        	else {
		                        		$scope.delivery = null;
		                        		$scope.delivery_idx = -1;

		                        		$scope.trigger = null;
		                        		$scope.trigger_idx = -1;
		                        	}
	                    		}
	                    		else {
	                    			httpService.loading('hide');
	                    			toastr.error(output.data.message, 'Error!');
	                    		}

		                        
	                    	});
                        }
                    	
                    },
                    btnClass: 'btn-danger'
                },
                No: {}
            }
        });
	}


	$scope.selectTrigger = function (idx) { //For trigger selection
		$scope.trigger = $scope.selected_report.triggers[idx];
		$scope.trigger_idx = idx;

		if($scope.trigger.templateName == 'Day Time Trigger') {
			var days = $scope.trigger.attributes[0].subAttribute;
			var days_arr = [];
			for (i in days) {
				if(days[i].name == 'Monday') {
					days_arr[0] = days[i];
				}
				else if(days[i].name == 'Tuesday') {
					days_arr[1] = days[i];
				}
				else if(days[i].name == 'Wednesday') {
					days_arr[2] = days[i];
				}
				else if(days[i].name == 'Thursday') {
					days_arr[3] = days[i];
				}
				else if(days[i].name == 'Friday') {
					days_arr[4] = days[i];
				}
				else if(days[i].name == 'Saturday') {
					days_arr[5] = days[i];
				}
				else if(days[i].name == 'Sunday') {
					days_arr[6] = days[i];
				}
			}

			$scope.trigger.attributes[0].subAttribute = days_arr;
		}
	}

	

	$scope.addTrigger = function (k, t) { //For adding of trigger

		var trigger_obj = {
			id : '0',
			name : '',
			path : '',
			templateName : k,
			webId : '',
			isDeleted : '0',
		};
		if(t == 'Time - Selected Days') {
			trigger_obj.attributes = [
				{
					id : '0',
					name : 'Days',
					path : '',
					value : '',
					webId: '',
					subAttribute : [
						{ 
							id : '0', 
							name :  'Monday',
							value : 'False'
						},
						{ 
							id : '0', 
							name :  'Tuesday',
							value : 'False'
						},
						{ 
							id : '0', 
							name :  'Wednesday',
							value : 'False'
						},
						{ 
							id : '0', 
							name :  'Thursday',
							value : 'False'
						},
						{ 
							id : '0', 
							name :  'Friday',
							value : 'False'
						},
						{ 
							id : '0', 
							name :  'Saturday',
							value : 'False'
						},
						{ 
							id : '0', 
							name :  'Sunday',
							value : 'False'
						}
					]
				},
				{
					id : '0',
					name : 'IsEnabled',
					type : 'Boolean',
					value : 'True'
				},
				{
					id : '0',
					name : 'Last Run Time',
					type : 'String',
					value : ''
				},
				{
					id : '0',
					name : 'Send Times',
					type : 'String',
					value : ''
				},

			];
		}
		else if(t == 'Event Frame') {
			trigger_obj.attributes = [
				{
					id : '0',
					name : 'AF Database Configuration',
					path : '',
					type : 'EnumerationValue',
					path : '',
					subAttribute : [],
					typeQualifier : 'AF Databases',
					value : '0',
					webId : ''
				},
				{
					id : '0',
					name : 'Event Frame Template',
					path : '',
					type : 'String',
					path : '',
					subAttribute : [],
					typeQualifier : '',
					value : '',
					webId : ''
				},
				{
					id : '0',
					name : 'IsEnabled',
					path : '',
					type : 'String',
					path : '',
					subAttribute : [],
					typeQualifier : '',
					value : 'True',
					webId : ''
				},
				{
					id : '0',
					name : 'TriggerOn',
					path : '',
					type : 'EnumerationValue',
					path : '',
					subAttribute : [],
					typeQualifier : 'TriggerOn',
					value : '0',
					webId : ''
				},
			];
		}
		else if(t == 'Time - Period & Offset') {
			trigger_obj.attributes = [
				{
					id : '0',
					name : 'IsEnabled',
					path : '',
					type : 'Boolean',
					path : '',
					typeQualifier : '',
					value : 'True',
					webId : ''
				},
				{
					id : '0',
					name : 'Last Run Time',
					path : '',
					type : 'String',
					path : '',
					subAttribute : [],
					typeQualifier : '',
					value : '',
					webId : ''
				},
				{
					id : '0',
					name : 'Offset',
					path : '',
					type : 'Int32',
					path : '',
					subAttribute : [],
					typeQualifier : '',
					value : '0',
					webId : '',
					subAttribute : [
						{
							id : '0',
							name : 'Time UOM',
							path : '',
							type : 'EnumerationValue',
							typeQualifier : 'Time UOM',
							value : '0',
							webId : ''
						}
					]
				},
				{
					id : '0',
					name : 'Period',
					path : '',
					type : 'Int32',
					path : '',
					subAttribute : [],
					typeQualifier : '',
					value : '0',
					webId : '',
					subAttribute : [
						{
							id : '0',
							name : 'Time UOM',
							path : '',
							type : 'EnumerationValue',
							typeQualifier : 'Time UOM',
							value : '0',
							webId : ''
						}
					]
				},
			];	
		}
		else if(t == 'Condition') {
			trigger_obj.attributes = [
				{
					id : '0', name : 'IsEnabled', path : '', type : 'Boolean', path : '', subAttribute : [], typeQualifier : '', value : 'True', webId : ''
				},
				{
					id : '0', name : 'Operator', path : '', type : 'EnumerationValue', path : '', subAttribute : [], typeQualifier : 'Operators', value : '0', webId : ''
				},
				{
					id : '0', name : 'PI Tag', path : '', type : 'Double', path : '', subAttribute : [], typeQualifier : '', value : '', webId : ''
				},
				{
					id : '0', name : 'Trigger State', path : '', type : 'Double', path : '', subAttribute : [], typeQualifier : '', value : '', webId : ''
				},
			];
		}


		$scope.selected_report.triggers.push(trigger_obj);

		//Setting the newly added trigger as active
		var trigger_idx = $scope.selected_report.triggers.length - 1;
		$scope.trigger = $scope.selected_report.triggers[trigger_idx];
		$scope.trigger_idx = trigger_idx;

		$scope.scrollToBottom('trigger-table');
	}

	$scope.deleteTrigger = function (t, idx) {
		var confirm = $ngConfirm({
            title: false,
            content: '<strong>Are you sure?</strong>',
            type: 'red',
            buttons: {
                Yes: {
                    action: function (scope, button) {
                        if(t.id == 0) { //If the ID is 0 it means the records is not yet saved to the database
                        	$scope.selected_report.triggers.splice(idx, 1);
                        }
                        else { //If not equal to zero then mark as isDeleted to remove from the databse.
                        	$scope.selected_report.triggers[idx].isDeleted = 1;
                        }
                        
                    	$scope.trigger = null;
                    	$scope.trigger_idx = -1;
                        $scope.$apply();
                        
                    },
                    btnClass: 'btn-danger'
                },
                No: {}
            }
        });
	}

	$scope.triggerFormChanged = function (t) { //Checking if the trigger record has been updated
		t.isUpdated = 1;
	}

	$scope.triggerHover = function (idx) { //Mouseenter on trigger row
		$scope.trigger_hover_idx = idx;
	}

	$scope.triggerHoverout = function (idx) { //Mouseleaver on trigger row
		$scope.trigger_hover_idx = null;
	}

	$scope.checkTriggerName = function (name) {
		for (i in $scope.selected_report.triggers) {
			if($scope.selected_report.triggers[i].name.toUpperCase() == name.toUpperCase() && i != $scope.trigger_idx) {
				$ngConfirm({
		            title: false,
		            content: 'Trigger Name Already Exists',
		            type: 'red',
		            buttons: {
		                Yes: {
		                    action: function (scope, button) {
		                    	$scope.trigger.name = '';
		                    	$scope.$apply();
		                    },
		                    btnClass: 'btn-danger'
		                }
		            }
		        });
			}
		}
	}

	$scope.deliveryHover = function (idx) { //Mouseenter on delivery row
		$scope.delivery_hover_idx = idx;
	}

	$scope.deliveryHoverout = function () { //Mouseleave on delivery row
		$scope.delivery_hover_idx = null;
	}

	$scope.addDelivery = function (k, v) { //Adding new Delivery to delivery array
		var delivery_obj = {
			id : '0',
			name : '',
			path : '',
			templateName : k,
			webId : '',
			isDeleted : '0'
		};

		//Conditions depends on the AF structure of every element
		if(k == 'Email Delivery Channel') {
			delivery_obj.attributes = [
				{
					id : '0', name : 'Email Template Location', type : 'String', value : '0', path : ''
				},
				{
					id : '0', name : 'IsEnabled', type : 'Boolean', value : 'True', path : ''
				},
				{
					id : '0', name : 'Subject', type : 'String', value : '', path : ''
				},
				{
					id : '0', name : 'To', type : 'String', value: '', path: ''
				}
			];
		}
		else if(k == 'Originating Event Frame Delivery Channel') {
			delivery_obj.attributes = [
				{
					id : '0', name : 'Comment', type : 'String', value : '', path : ''
				},
				{
					id : '0', name : 'IsEnabled', type : 'Boolean', value : 'True', path : ''
				}
			];
		}
		else if(k == 'Folder Delivery Channel') {
			delivery_obj.attributes = [
				{
					id : '0', name : 'Custom Delivery Destination', type : 'String', value : '', path : ''
				},
				{
					id : '0', name : 'IsEnabled', type : 'Boolean', value : 'True', path : ''
				},
				{
					id : '0', name : 'Server Share Destination', type : 'String', value : '0', path : ''
				}
			];
		}
		else if(k == 'Sharepoint Delivery Channel') {
			delivery_obj.attributes = [
				{
					id : '0', name : 'IsEnabled', type : 'Boolean', value : 'True', path : ''
				},
				{
					id : '0', name : 'Sharepoint Document Library Destination', type : 'String', value : '', path : ''
				}
			];
		}

		$scope.selected_report.delivery.push(delivery_obj); //Adding the delivery object to deliveries array
		var delivery_idx = $scope.selected_report.delivery.length - 1; //Getting the last delivery index
		$scope.delivery = $scope.selected_report.delivery[delivery_idx]; //make the newly added delivery selected
		$scope.delivery_idx = delivery_idx;

		$scope.scrollToBottom('delivery-table');

	}

	$scope.deleteDelivery = function (d, idx) {

		var confirm = $ngConfirm({
            title: false,
            content: '<strong>Are you sure?</strong>',
            type: 'red',
            buttons: {
                Yes: {
                    action: function (scope, button) {
                    	if(d.id == 0) {
                    		$scope.selected_report.delivery.splice(idx, 1);
                    	}
                    	else {
                    		$scope.selected_report.delivery[idx].isDeleted = 1;
                    	}
                    	
                    	$scope.delivery = null;
	                    $scope.delivery_idx = -1;

	                    $scope.$apply();
                    },
                    btnClass: 'btn-danger'
                },
                No: {}
            }
        });
	}


	$scope.saveReport = function () {
		console.log($scope.selected_report);
		httpService.loading('show');
		httpService.saveReport($scope.selected_report).then(function (output) {

			var response = output.data;
			if(response.status == 'failed') {
				httpService.loading('hide');
				toastr.error(response.message, 'Failed!');
			}
			else {
				var idx = $scope.selected_report_idx; //Getting the array index of the selected report
				$scope.reports[idx] = output.data; //Overwriting the report record to a valid report data returned by the server
				$scope.selected_report = $scope.reports[idx];

				httpService.loading('hide');

				$scope.generalForm.$setPristine();
	            $scope.triggerForm.$setPristine(); 
	            $scope.deliveryForm.$setPristine();
	            $scope.has_pending_report = false;

				toastr.success('Report has been saved.', 'Success!');
			}
			
		}, function (err) {
			httpService.loading('hide');
			toastr.error('Something went wrong, please try again.', 'Error!');
		});
	}

	$scope.selectDelivery = function (d, idx) {
		$scope.delivery = d;
		$scope.delivery_idx = idx;
	}

	$scope.deliveryFormChange = function (d) { //When the delivery record has been modified
		d.isUpdated = 1;
	}

	$scope.checkDeliveryName = function (name) {
		for(i in $scope.selected_report.delivery) {
			if($scope.selected_report.delivery[i].name.toUpperCase() == name.toUpperCase() && $scope.delivery_idx != i) {
				if($scope.selected_report.delivery[i].isDeleted == 0) {
					$ngConfirm({
			            title: false,
			            content: 'Delivery Name Already Exists',
			            type: 'red',
			            buttons: {
			                Yes: {
			                    action: function (scope, button) {
			                    	$scope.delivery.name = '';
			                    	$scope.$apply();
			                    },
			                    btnClass: 'btn-danger'
			                }
			            }
			        });
				}
			}
		}
		return true;
	}

	$scope.getStatus = function (d) {
		var attributes = d.attributes;
		
		for(i in attributes) {
			if(attributes[i].name == 'IsEnabled' && attributes[i].value == "True") {
				return true;
			}
		}

		return false;
	}

	$scope.scrollToBottom = function (id) {
		$timeout(function () {
            $('#' + id + ' .scrollArea').animate({ scrollTop: $('#'+ id +' .scrollArea')[0].scrollHeight }, 500);
        });
	}


}])