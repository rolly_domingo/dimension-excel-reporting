angular.module('reportingApp', ['reportingApp.reportingController', 'reportingApp.directives','angular-loading-bar', 'scrollable-table', 'toastr', 'cp.ngConfirm', 'ui.bootstrap', 'ngAnimate', 'ngSanitize'])
.service('httpService', ['$http', 'HOST', function ($http, HOST) {
	return {
		'test' : function ($url) {
			return $url;
		},
		'getInitialData' : function () {
			return $http({
				url : HOST + '/getInitialData',
				method : 'GET'
			});
		},
		saveReport : function (data) {
			return $http({
				url : HOST + '/CreateAndModifyRecord',
				method : 'POST',
				data : $.param(data),
				headers : {'Content-Type' : 'application/x-www-form-urlencoded'}
			})
		},
		deleteReport : function (web_id) {
			return $http({
				url : HOST + '/DeleteReport',
				params : {webId : web_id},
				method : 'GET'
			})
		},
		'loading' : function (action) {
			$('#loading').modal(action);
		}
	}
}])
